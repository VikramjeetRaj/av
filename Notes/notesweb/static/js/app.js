var app = angular.module('notesapp', []);
app.controller('homeCtrl', function ($scope, $http) {

    $scope.notesData = {
        'name' : 'Edit Name',
        'content' : 'Edit Content'
    };

    $scope.fetchNotes = function () {
        var p_url = "/api/notes/";
        $http.get(p_url, { withCredentials: true }).then(function (data) {
            if(data.status == 200) {
                $scope.notes = data.data;
            } else {
                alert("Some Error Occured");
            }

        }, function (error) { console.log(error);});
    }
    
    setInterval(function(){ $scope.fetchNotes() }, 30000)

    $scope.onInit = function(){
        $scope.fetchNotes();
    }();

    $scope.showEditPopup = function(note) {
        $scope.notesData = note;
    }

    $scope.delete = function(note) {
        if (confirm("Delete?")){
            $scope.notesData = note;
            $scope.postNote('Are you sure you want to delete ' + note.name +'?');
        }
    }

    $scope.postNote = function(type) {
        var p_url = "/api/notes/";
        $scope.notesData.type = type;
        if($scope.notesData.name.substr($scope.notesData.name.length - 4) != '.txt') {
            $scope.notesData.name += ".txt";
        }
        var p_data = $scope.notesData;
        $http.post(p_url, p_data, { withCredentials: true }).then(function (data) {
            if(data.status == 200) {
                alert("Successful! This note will show after 30s. Please Wait!")
            } else {
                alert("Some Error Occured");
            }

        }, function (error) { console.log(error);});
    }
});