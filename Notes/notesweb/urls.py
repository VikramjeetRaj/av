from django.conf.urls import url, include
from notesweb.views import HomeView, NotesAPI, LogoutView
from rest_framework.urlpatterns import format_suffix_patterns
from django.contrib.auth.views import login
urlpatterns = [
    url(r'^auth/complete/google-oauth2/notesweb/home', HomeView.as_view()),
    url(r'^api/notes', NotesAPI.as_view()),
    url(r'^user/logout', LogoutView.as_view()),
]
urlpatterns = format_suffix_patterns(urlpatterns)
