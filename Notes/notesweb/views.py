
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView, View
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
from rest_framework.views import APIView
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework import permissions
from rest_framework.response import Response
import json
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from django.contrib.auth import logout
from django.http import HttpResponse

class LogoutView(View):
  def get(self, request):
    logout(request)
    return HttpResponse('Bye! See you soon')

class CsrfExemptSessionAuthentication(SessionAuthentication):
    '''
    To remove csrf token requirement from rest based endpoints
    '''

    def enforce_csrf(self, request):
        return


class HomeView(TemplateView):
    template_name = "home/home.html"

class NotesAPI(APIView):
  authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)  
  gauth = GoogleAuth()
  drive = GoogleDrive(gauth)
  def get(self, request):
    notes_list = []
    file_list = self.drive.ListFile({'q': "'root' in parents and trashed=false"}).GetList()
    for file1 in file_list:
      if ".txt" in file1['title']:
        notes = {}
        print(file1.GetContentString(mimetype=None))
        print('title: %s, id: %s' % (file1['title'], file1['id']))
        notes['name'] = file1['title']
        notes['id'] = file1['id']
        notes['content'] = file1.GetContentString(mimetype=None)
        notes_list.append(notes)
    return Response(notes_list)
  def post(self, request, format = None):
    request_data = request.data
    operated_file = []
    if request_data['type'] == 'update':
      file_list = self.drive.ListFile({'q': "'root' in parents and trashed=false"}).GetList()
      for file1 in file_list:
        if file1['id'] == request_data['id']:
          file1.SetContentString(request_data['content'])
          file1.Upload()
          operated_file.append(file1)
    if request_data['type'] == 'delete':
      file_list = self.drive.ListFile({'q': "'root' in parents and trashed=false"}).GetList()
      for file1 in file_list:
        if file1['id'] == request_data['id']:
          file1.Trash()
          operated_file.append(file1)
    else:
      file = self.drive.CreateFile({'title': request_data['name']})
      file.SetContentString(request_data['content'])
      file.Upload()
      operated_file.append(file)
    return Response(operated_file)
